import Vue from 'vue'
import Router from 'vue-router'
import zHome from '@/components/container/home'
import dashboard from '@/components/page/dashboard'
import basicechart from '@/components/page/basicechart'
import todolist from '@/components/component/todolist'
import tables from '@/components/page/tables'
import formlayouts from '@/components/page/formlayouts'
import formInput from '@/components/page/formInput'
// 用户页面start
import notFind from '@/components/userpage/notFind'
import error from '@/components/userpage/error'
import forget from '@/components/userpage/forget'
import login from '@/components/userpage/login'
import reg from '@/components/userpage/reg'
// 用户页面end
// 组件start
import animation from '@/components/component/animation'
import navmenu from '@/components/component/navmenu'
import tabs from '@/components/component/tabs'
import editor from '@/components/component/editor'
// 组件end
Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: login,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/zHome',
      name: 'zHome',
      component: zHome,
      meta: {
        requiresAuth: true
      },
      children: [
        {
          path: '/dashboard',
          component: dashboard,
          meta: {
            requiresAuth: true
          }
        },
        {
          path: '/basicechart',
          component: basicechart,
          meta: {
            requiresAuth: true
          }
        },
        {
          path: '/todolist',
          component: todolist,
          meta: {
            requiresAuth: true
          }
        },
        {
          path: '/animation',
          component: animation,
          meta: {
            requiresAuth: true
          }
        },
        {
          path: '/navmenu',
          component: navmenu,
          meta: {
            requiresAuth: true
          }
        },
        {
          path: '/tabs',
          component: tabs,
          meta: {
            requiresAuth: true
          }
        },
        {
          path: '/tables',
          component: tables,
          meta: {
            requiresAuth: true
          }
        },
        {
          path: '/editor',
          component: editor,
          meta: {
            requiresAuth: true
          }
        },
        {
          path: '/formlayouts',
          component: formlayouts,
          meta: {
            requiresAuth: true
          }
        },
        {
          path: '/formInput',
          component: formInput,
          meta: {
            requiresAuth: true
          }
        }
      ]
    },
    // 用户页面start
    {
      path: '/notFind',
      component: notFind
    },
    {
      path: '/error',
      component: error
    },
    {
      path: '/forget',
      component: forget
    },
    {
      path: '/reg',
      component: reg
    }
    // 用户页面end
  ]
})
